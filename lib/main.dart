import 'package:page_transition/page_transition.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:module_4_app/sign_in.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';

void main() {
  runApp(const Module4());
}

class Module4 extends StatelessWidget {
  const Module4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: AnimatedSplashScreen(
          duration: 1000,
          splash: 'assets/nature-logo.png',
          nextScreen: const SignInPage(),
          splashTransition: SplashTransition.slideTransition,
          pageTransitionType: PageTransitionType.bottomToTop),
      theme: FlexThemeData.light(
        scheme: FlexScheme.green,
        appBarOpacity: 0.68,
        appBarElevation: 2.5,
        subThemesData: const FlexSubThemesData(
          blendOnLevel: 35,
          blendOnColors: false,
          defaultRadius: 27.0,
          textButtonRadius: 7.0,
          elevatedButtonRadius: 9.0,
          outlinedButtonRadius: 9.0,
          elevatedButtonSchemeColor: SchemeColor.primaryContainer,
          inputDecoratorSchemeColor: SchemeColor.onSecondaryContainer,
          inputDecoratorRadius: 35.0,
          dialogRadius: 6.0,
          timePickerDialogRadius: 6.0,
          bottomNavigationBarSelectedLabelSchemeColor:
              SchemeColor.onSecondaryContainer,
          bottomNavigationBarUnselectedLabelSchemeColor:
              SchemeColor.onSecondaryContainer,
          bottomNavigationBarSelectedIconSchemeColor:
              SchemeColor.onSecondaryContainer,
          bottomNavigationBarUnselectedIconSchemeColor:
              SchemeColor.onSecondaryContainer,
          bottomNavigationBarOpacity: 0.20,
          bottomNavigationBarElevation: 9.5,
          navigationBarOpacity: 0.69,
          navigationRailOpacity: 0.57,
        ),
        visualDensity: FlexColorScheme.comfortablePlatformDensity,
        useMaterial3: true,
      ),
     debugShowCheckedModeBanner: false,
    );
  }
}
